package com.clientui.clientui.controller;

import com.clientui.clientui.beans.CommandeBean;
import com.clientui.clientui.beans.ProductBean;
import com.clientui.clientui.proxies.MicroserviceProduitProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class ClientController {

    @Autowired
    MicroserviceProduitProxy mProduitsProxy;

    @RequestMapping("/")
    public String accueil(Model model){

        List<ProductBean> produits = mProduitsProxy.listeDesProduits();
        model.addAttribute("produits",produits);
        return "Accueil";
    }

    @RequestMapping("/details-produit/{id}")
    public String ficheProduit(@PathVariable int id, Model model){

        ProductBean produit = mProduitsProxy.recupererUnProduit(id);

        model.addAttribute("produit", produit);

        return "FicheProduit";
    }

    @RequestMapping("/details-produit/commander-produit/{id}")
    public String formulaireCommande(@PathVariable int id, @RequestParam(name = "quantite",defaultValue = "") String quantite,Model model1){
        CommandeBean commande = new CommandeBean();

        model1.addAttribute("commande",commande);
        model1.addAttribute("quantite",quantite);
        return "FormulaireCommande";
    }

    @RequestMapping("/commande/{quantite}")
    public String quantiteCommande(@PathVariable String quantite, Model model){
     System.out.println("%"+quantite+"%");
        return "Accueil";
    }
}
